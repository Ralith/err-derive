extern crate rustc_version;

use rustc_version::version;

fn version_ge(minor_min: u64) -> bool {
    let version = version().unwrap();

    version.major == 1 && version.minor >= minor_min
}

fn conditional_cfg(minor_min: u64, cfg: &str) {
    if version_ge(minor_min) {
        println!("cargo:rustc-cfg={}", cfg);
    }
}

fn main() {
    if !version_ge(15) {
        panic!("`err-derive` needs support for custom derive, which requires rustc >= 1.15");
    }

    conditional_cfg(30, "RUSTC_SUPPORTS_STD_ERROR_SOURCE");
}
